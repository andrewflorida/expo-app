import { createSlice } from '@reduxjs/toolkit';
import AuthService from '../localStorage';
// import history from '../lib/history';
import firebase from '../base';

export const initialState = {
    loading: false,
    hasErrors: false,
    data: [],
};

const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        loginUser: (state) => {
            state.loading = true;
        },
        loginUserSuccess: (state, { payload }) => {
            state.data = payload;
            state.loading = false;
            state.hasErrors = false;
        },
        loginUserFailure: (state) => {
            state.loading = false;
            state.hasErrors = true;
        },
        logoutUser: (state) => {
            state.loading = true;
        },
        logoutUserSuccess: (state) => {
            state.data = [];
            state.loading = false;
            state.hasErrors = false;
        },
        logoutUserFailure: (state) => {
            state.loading = false;
            state.hasErrors = true;
        },
        registerUser: (state) => {
            state.loading = true;
        },
        registerUserSuccess: (state) => {
            state.loading = false;
            state.hasErrors = false;
        },
        registerUserFailure: (state) => {
            state.loading = false;
            state.hasErrors = true;
        },
        loginWithGoogle: (state) => {
            state.loading = true;
        },
        loginWithGoogleSuccess: (state) => {
            state.loading = false;
            state.hasErrors = false;
        },
        loginWithGoogleFailure: (state) => {
            state.loading = false;
            state.hasErrors = true;
        },
    },
});

export const {
    loginUser, loginUserSuccess, loginUserFailure, logoutUser, logoutUserSuccess,
    logoutUserFailure, registerUser, registerUserSuccess, registerUserFailure,
    loginWithGoogle, loginWithGoogleSuccess, loginWithGoogleFailure,
} = userSlice.actions;

export const userSelector = state => state.user;

export default userSlice.reducer;

export function fetchLoginWithGoogle() {
    return (dispatch) => {
        dispatch(loginWithGoogle());

        const provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(
            (res) => {
                const data = {
                    email: res.user.email,
                    refreshToken: res.user.refreshToken,
                };
                AuthService.authenticateUser(true, data);
                dispatch(loginWithGoogleSuccess(data));
                //history.push('/');
            },
            () => dispatch(loginWithGoogleFailure()),
        );
    };
}

export function fetchLoginUser({ email, password }) {
    return (dispatch) => {
        dispatch(loginUser());
        firebase.auth().signInWithEmailAndPassword(email, password).then(
            (res) => {
                const data = {
                    email: res.user.email,
                    refreshToken: res.user.refreshToken,
                };
                AuthService.authenticateUser(true, data);
                dispatch(loginUserSuccess(data));
                //history.push('/');
            },
            () => dispatch(loginUserFailure()),
        );
    };
}

export function fetchLogoutUser() {
    return (dispatch) => {
        dispatch(logoutUser());
        firebase.auth().signOut().then(
            () => {
                AuthService.deauthenticateUser();
                dispatch(logoutUserSuccess());
                //history.push('/login');
            },
            () => dispatch(logoutUserFailure()),
        );
    };
}

export function fetchRegisterNewUser({ email, password }) {
    return (dispatch) => {
        dispatch(registerUser());
        firebase.auth().createUserWithEmailAndPassword(email, password).then(
            (res) => {
                const data = {
                    email: res.user.email,
                    refreshToken: res.user.refreshToken,
                };
                AuthService.authenticateUser(true, data);
                dispatch(loginUserSuccess(data));
                //history.push('/');
            },
            () => dispatch(loginUserFailure()),
        );
    };
}
