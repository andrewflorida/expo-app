import { createSlice } from '@reduxjs/toolkit';
import { isEmpty } from 'lodash';
import { firebaseStorage, firestoreDB } from '../base';
import { transformOne, transformResponseToObj } from '../lib/helpers';

// import history from '../lib/history';

export const initialState = {
    loading: false,
    hasErrors: false,
    data: [],
    one: {},
};

const listingSlice = createSlice({
    name: 'listings',
    initialState,
    reducers: {
        getListing: (state) => {
            state.loading = true;
        },
        getListingSuccess: (state, { payload }) => {
            state.data = payload;
            state.loading = false;
            state.hasErrors = false;
        },
        getListingFailure: (state) => {
            state.loading = false;
            state.hasErrors = true;
        },
        getOneListing: (state) => {
            state.loading = true;
        },
        getOneListingSuccess: (state, { payload }) => {
            state.one = payload;
            state.loading = false;
            state.hasErrors = false;
        },
        getOneListingFailure: (state) => {
            state.loading = false;
            state.hasErrors = true;
        },
        editListing: (state) => {
            state.loading = true;
        },
        editListingSuccess: (state) => {
            state.loading = false;
            state.hasErrors = false;
        },
        editListingFailure: (state) => {
            state.loading = false;
            state.hasErrors = true;
        },
        createListing: (state) => {
            state.loading = true;
        },
        createListingSuccess: (state) => {
            state.loading = false;
            state.hasErrors = false;
        },
        createListingFailure: (state) => {
            state.loading = false;
            state.hasErrors = true;
        },
        deleteListing: (state) => {
            state.loading = true;
        },
        deleteListingSuccess: (state) => {
            state.loading = false;
            state.hasErrors = false;
        },
        deleteListingFailure: (state) => {
            state.loading = false;
            state.hasErrors = true;
        },
    },
});

export const {
    getListing, getListingSuccess, getListingFailure,
    getOneListing, getOneListingSuccess, getOneListingFailure,
    editListing, editListingSuccess, editListingFailure,
    createListing, createListingSuccess, createListingFailure,
    deleteListing, deleteListingSuccess, deleteListingFailure,
} = listingSlice.actions;

export const listingsSelector = state => state.listings;

export default listingSlice.reducer;

export function fetchGetListing() {
    return (dispatch) => {
        dispatch(getListing());
        firestoreDB.collection('listings').get().then(
            (res) => {
                const correctRes = transformResponseToObj(res);
                dispatch(getListingSuccess(correctRes));
            },
            () => dispatch(getListingFailure()),
        );
    };
}

export function fetchGetOneListing(id) {
    return (dispatch) => {
        dispatch(getOneListing());
        firestoreDB.collection('listings').doc(id).get().then(
            (res) => {
                const correctRes = transformOne(res);
                dispatch(getOneListingSuccess(correctRes));
            },
            () => dispatch(getOneListingFailure()),
        );
    };
}

const saveFiles = (files, id) => {
    const storageRef = firebaseStorage.ref();

    return Promise.all([...files].map(async (file) => {
        const fileRef = storageRef.child(`image/${id}${file.name}`);
        await fileRef.put(file);
        return fileRef.getDownloadURL();
    }));
};

export function fetchEditListing({ images, primaryImage, ...formData }, id) {
    return (dispatch) => {
        dispatch(editListing());
        if (!isEmpty(primaryImage) && !isEmpty(primaryImage)) {
            saveFiles([...primaryImage, ...images], id)
                .then(
                    ([primaryImageUrl, ...imagesURL]) => firestoreDB.collection('listings').doc(id)
                        .update({
                            ...formData,
                            primaryImage: primaryImageUrl,
                            images: imagesURL,
                        }),
                    () => dispatch(editListingFailure()),
                ).then(
                () => {
                    dispatch(editListingSuccess());
                    // //history.push('/home');
                },
                () => dispatch(editListingFailure()),
            );
        } else if (!isEmpty(primaryImage)) {
            saveFiles(primaryImage, id)
                .then(
                    ([primaryImageUrl]) => firestoreDB.collection('listings').doc(id)
                        .update({
                            ...formData,
                            primaryImage: primaryImageUrl,
                            images: [],
                        }),
                    () => dispatch(editListingFailure()),
                ).then(
                () => {
                    dispatch(editListingSuccess());
                    //history.push('/home');
                },
                () => dispatch(editListingFailure()),
            );
        } else if (!isEmpty(images)) {
            saveFiles(images, id)
                .then(
                    imagesURL => firestoreDB.collection('listings').doc(id)
                        .update({
                            ...formData,
                            primaryImage: '',
                            images: imagesURL,
                        }),
                    () => dispatch(editListingFailure()),
                ).then(
                () => {
                    dispatch(editListingSuccess());
                    //history.push('/home');
                },
                () => dispatch(editListingFailure()),
            );
        } else {
            firestoreDB.collection('listings').doc(id)
                .update({
                    ...formData,
                    primaryImage: '',
                    images: [],
                }).then(
                () => {
                    dispatch(editListingSuccess());
                    //history.push('/home');
                },
                () => dispatch(editListingFailure()),
            );
        }
    };
}

export function fetchCreateListing({ images, primaryImage, ...formData }) {
    return (dispatch) => {
        dispatch(createListing());
        firestoreDB.collection('listings').add({ ...formData, images: [], primaryImage: '' })
            .then(
                (res) => {
                    if (!isEmpty(primaryImage) && !isEmpty(primaryImage)) {
                        saveFiles([...primaryImage, ...images], res.id)
                            .then(
                                ([primaryImageUrl, ...imagesURL]) => firestoreDB.collection('listings')
                                    .doc(res.id)
                                    .update({
                                        primaryImage: primaryImageUrl,
                                        images: imagesURL,
                                    }),
                                () => dispatch(createListingFailure()),
                            )
                            .then(
                                () => {
                                    dispatch(createListingSuccess());
                                    //history.push('/home');
                                },
                                () => dispatch(createListingFailure()),
                            );
                    } else if (!isEmpty(primaryImage)) {
                        saveFiles(primaryImage, res.id)
                            .then(
                                ([primaryImageUrl]) => firestoreDB.collection('listings')
                                    .doc(res.id)
                                    .update({ primaryImage: primaryImageUrl }),
                                () => dispatch(createListingFailure()),
                            )
                            .then(
                                () => {
                                    dispatch(createListingSuccess());
                                    //history.push('/home');
                                },
                                () => dispatch(createListingFailure()),
                            );
                    } else if (!isEmpty(images)) {
                        saveFiles(images, res.id)
                            .then(
                                imagesUrl => firestoreDB.collection('listings')
                                    .doc(res.id)
                                    .update({ images: imagesUrl }),
                                () => dispatch(createListingFailure()),
                            )
                            .then(
                                () => {
                                    dispatch(createListingSuccess());
                                    //history.push('/home');
                                },
                                () => dispatch(createListingFailure()),
                            );
                    } else {
                        dispatch(createListingSuccess());
                        //history.push('/home');
                    }
                },
                () => dispatch(createListingFailure()),
            );
    };
}

export function fetchDeleteListing(id) {
    return (dispatch) => {
        dispatch(deleteListing());
        firestoreDB.collection('listings').doc(id).delete().then(
            () => {
                dispatch(deleteListingSuccess());
                dispatch(fetchGetListing());
                //history.push('/home');
            },
            () => dispatch(deleteListingFailure()),
        );
    };
}
