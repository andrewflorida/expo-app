import { combineReducers } from 'redux';

import usersReducer from './users';
import listingsReducer from './listings';

const rootReducer = combineReducers({
    users: usersReducer,
    listings: listingsReducer,
});

export default rootReducer;
