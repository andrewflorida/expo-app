const PROJECT = 'ads_test';

export default class AuthService {
  /**
   * Set `isLogged` && 'UserData' to the localStorage
   * @param {Boolean | String} active - user logged flag;
   * @param userData
   */
  static authenticateUser(active, userData) {
    localStorage.setItem(`${PROJECT}_IsLogged`, active);
    localStorage.setItem(`${PROJECT}_UserData`, JSON.stringify(userData));
  }

  /**
   * clear `isLogged` && 'UserData' from the localStorage
   */
  static deauthenticateUser() {
    localStorage.removeItem(`${PROJECT}_IsLogged`);
    localStorage.removeItem(`${PROJECT}_UserData`);
  }

  /**
   * @returns {String} IsLogged `undefined | true`
   */
  static isUserAuthenticated() {
    return localStorage.getItem(`${PROJECT}_IsLogged`);
  }

  static getUserData() {
    return JSON.parse(localStorage.getItem(`${PROJECT}_UserData`));
  }

  static getUserRole() {
    const { role = null } = localStorage.getItem(`${PROJECT}_UserData`)
      ? JSON.parse(localStorage.getItem(`${PROJECT}_UserData`)) : {};
    return role;
  }
}
