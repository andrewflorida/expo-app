const fieldForListing = [
  'listingType',
  'propertyType',
  'images',
  'primaryImage',
  'address',
  'location',
  'size',
  'beds',
  'baths',
  'price',
];

export const transformResponseToObj = (res) => {
  const list = [];
  res.forEach((doc) => {
    const item = fieldForListing.reduce((result, field) => {
      // console.log(result)
      return { ...result, [field]: doc.get(field) };
    }, { id: doc.id });
    list.push(item);
  });
  return list;
};

export const transformOne = (res) => {
  return fieldForListing.reduce((result, field) => {
    // console.log(result)
    return { ...result, [field]: res.get(field) };
  }, { id: res.id });
};
