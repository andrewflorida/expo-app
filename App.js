import {StatusBar} from 'expo-status-bar';
import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import { configureStore } from '@reduxjs/toolkit';
import { Provider as PaperProvider } from 'react-native-paper';
import { Provider as StoreProvider } from 'react-redux';

import rootReducer from './src/reducers';

const store = configureStore({ reducer: rootReducer });

export default function App() {
    return (
        <StoreProvider store={store}>
            <PaperProvider>
                <View style={styles.container}>
                    <Text>Test up App.js to start working on your app!</Text>
                    <StatusBar style="auto"/>
                </View>
            </PaperProvider>
        </StoreProvider>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
